﻿import * as React from 'react';
import { connect } from 'react-redux';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { ApplicationState } from '../../store';
import * as LoginState from '../../store/Login';

type LoginProps =
    LoginState.LoginState     // ... state we've requested from the Redux store
    & typeof LoginState.actionCreators   // ... plus action creators we've requested
    & { params: { email: string } }; 

class LoginCard extends React.Component<LoginProps, {}> {
    constructor(props) {
        super(props);

    }


    public render() {
        return <Card>
            <CardTitle title="Login" subtitle="" />
            <form onSubmit={(e) => {
                e.preventDefault();
                const emailValue = (document.querySelector('#emailInput') as HTMLInputElement).value;
                this.props.requestLogin(emailValue);
            }}>
            <CardText>
                
                    <TextField
                        id="emailInput"
                           hintText="Enter an email address"
                           floatingLabelText="Email"
                           type="email"
                           defaultValue={this.props.email}
                       /><br />
                       
                   </CardText>
                   <CardActions>
                    <RaisedButton label="Login" primary={true} type="submit" />
                   </CardActions>
                   </form>
               </Card>;
    }
}
export default connect(
    (state: ApplicationState) => state.login, // Selects which state properties are merged into the component's props
    LoginState.actionCreators                 // Selects which action creators are merged into the component's props
)(LoginCard);