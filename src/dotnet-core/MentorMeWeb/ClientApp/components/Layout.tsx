import * as React from 'react';

import { SideMenu } from './menu/SideMenu';
import {NavMenu} from './menu/NavMenu'
export interface LayoutProps {
    body: React.ReactElement<any>;
}

export class Layout extends React.Component<LayoutProps, void> {
    public render() {
        return <div className={"mdl-layout mdl-js-layout mdl-layout--fixed-header"}>
                   <NavMenu/>
                   <SideMenu/>
                   <main className={"mdl-layout__content"}>
                       <div className={"page-content"}>
                           {this.props.body}
                       </div>
                   </main>
               </div>;
    }
}
