﻿import * as React from 'react';
import {connect} from 'react-redux'
import { ApplicationState } from '../store';
import * as TagsState from '../store/Tags';

type TagsStateProps =
    TagsState.TagsState     // ... state we've requested from the Redux store
    & typeof TagsState.actionCreators   // ... plus action creators we've requested 

class Home extends React.Component<TagsStateProps, {}> {
    componentWillMount() {
        // This method runs when the component is first added to the page
        this.props.requestTags();
    }
    public render() {
        return <div className={"mdl-grid"}>
                   <div className={"mdl-cell mdl-cell--2-col"}>
                      
                   </div>
                   <div className={"mdl-cell mdl-cell--8-col"}>
                      Search results
                   </div>
                   <div className={"mdl-cell mdl-cell--2-col"}>
                   </div>
               </div>;
    }
}
export default connect(
    (state: ApplicationState) => state.tags, // Selects which state properties are merged into the component's props
    TagsState.actionCreators                 // Selects which action creators are merged into the component's props
)(Home);