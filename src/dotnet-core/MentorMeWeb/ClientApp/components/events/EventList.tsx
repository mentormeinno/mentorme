﻿import * as React from 'react';
import { connect } from 'react-redux';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import ActionInfo from 'material-ui/svg-icons/action/info';

import {
    blue300,
    indigo900,
    orange200,
    deepOrange300,
    pink400,
    purple500,
    white
    } from 'material-ui/styles/colors';
import { ApplicationState } from '../../store';
import * as EventsState from '../../store/UserEvents';

type EventsProps =
    EventsState.EventsState     // ... state we've requested from the Redux store
    & typeof EventsState.actionCreators   // ... plus action creators we've requested
    & {
        params: {
            userid: string
        }
    }; 

const style = { margin: 5 };

class EventList extends React.Component<EventsProps, {}> {
    constructor() {
        super();
    }

    public render() {

        return <Card>
                   <CardTitle title="My Events" subtitle="" />
                   <CardText>
            
                    <List>
                        <Subheader>Presentations</Subheader>
                        <ListItem
                            primaryText="Presentation 1"
                            secondaryText="The description for the presentation"
                            secondaryTextLines={2}
                            leftAvatar={<Avatar
                                            color={deepOrange300}
                                            backgroundColor={purple500}
                                            size={30}
                                            style={style}
                                        >
                                            P
                                        </Avatar>}
                            rightIcon={<ActionInfo />}
                        />
    
                        <ListItem
                        primaryText="Presentation 2"
                            leftAvatar={<Avatar
                                            color={deepOrange300}
                                            backgroundColor={purple500}
                                            size={30}
                                            style={style}
                                        >
                                            P
                                        </Avatar>}
                            rightIcon={<ActionInfo />}
                        />
                       </List>
                    <Divider />
                    <List>
                        <Subheader>Classrooms</Subheader>
                    <ListItem
                        primaryText="Classroom 1"
                        leftAvatar={<Avatar
                            color={white}
                                backgroundColor={blue300}
                                size={30}
                                style={style}
                            >
                                C
                                        </Avatar>}
                        rightIcon={<ActionInfo />}
                    />
                    </List>
   
                   </CardText>
               </Card>;
    }
}

export default connect(
    (state: ApplicationState) => state.events, // Selects which state properties are merged into the component's props
    EventsState.actionCreators                 // Selects which action creators are merged into the component's props
)(EventList);