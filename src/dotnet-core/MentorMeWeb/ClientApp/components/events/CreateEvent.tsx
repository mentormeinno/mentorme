﻿import * as React from 'react';
import {connect} from 'react-redux';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import AutoComplete from 'material-ui/AutoComplete';
import ChipInput from 'material-ui-chip-input'
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { ApplicationState } from '../../store';
import * as EventsState from '../../store/Events';
import * as TagsState from '../../store/Tags';

type SavedEventProps =
    TagsState.TagsState &
    EventsState.SaveEventState     // ... state we've requested from the Redux store
    & typeof EventsState.actionCreators   // ... plus action creators we've requested
    & {
        params: {
            evtype: string,
            name: string,
            tags: Array<string>,
            presenterid: string } }; 

class CreateEvent extends React.Component<SavedEventProps,{}> {
    constructor(props) {
        super(props);
        console.log(new Object(this.props.tags))
        this.handleChange = this.handleChange.bind(this);
    }
  
    handleChange = (event, index,value) => this.setState({value});
    public render() {
        
        return <Card>
                   <CardTitle title="Create Event" subtitle=""/>
                   <CardText>
                <SelectField
                    value={this.props.typeindex}
                    onChange={this.handleChange}
                    floatingLabelText="Type">
                    <MenuItem value={1} primaryText="Presentation" />
                    <MenuItem value={2} primaryText="Book club" />
                    <MenuItem value={3} primaryText="Code dojo" />
                </SelectField>
                <br />
                <TextField floatingLabelText="Name" />
                <br />
                <ChipInput
                    defaultValue={[]}
                    dataSource={this.props.tags}
                    filter={AutoComplete.caseInsensitiveFilter}
                />
                <br />
                   </CardText>
                   <CardActions>
                    <RaisedButton label="Create Event" primary={true}/>
                   </CardActions>
               </Card>;
    }
} export default connect(
    (state: ApplicationState) => state.savedevent, // Selects which state properties are merged into the component's props
    EventsState.actionCreators                 // Selects which action creators are merged into the component's props
)(CreateEvent);