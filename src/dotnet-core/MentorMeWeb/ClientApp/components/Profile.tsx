import * as React from 'react';
import IdentityCard from './profile/IdentityCard';
import BadgesCard from "./profile/BadgesCard";
import TagsCard from "./profile/TagsCard";
import EventsCard from "./profile/EventsCard";
import ConnectionsCard from "./profile/ConnectionsCard";

export default class Profile extends React.Component<{}, {}> {
    public render() {
        return <div className={"mdl-grid"}>
            <div className={"mdl-cell mdl-cell--2-col"}>
            <EventsCard/>
            </div>
                   <div className={"mdl-cell mdl-cell--8-col"}>
                <IdentityCard />
                <br/>
                <BadgesCard />
                <br />
                        <TagsCard />
                   </div>
                   <div className={"mdl-cell mdl-cell--2-col"}>
                       <ConnectionsCard />
            </div>
               </div>;
    }
}
