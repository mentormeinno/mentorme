﻿import * as React from 'react';
import IdentityCard from './profile/IdentityCard';
import BadgesCard from "./profile/BadgesCard";
import TagsCard from "./profile/TagsCard";
import EventsCard from "./profile/EventsCard";
import ConnectionsCard from "./profile/ConnectionsCard";
import RaisedButton from 'material-ui/RaisedButton';
import CreateEvent from "./events/CreateEvent";
import EventList from "./events/EventList";

export default class Events extends React.Component<{}, {}> {
    public render() {
        return <div className={"mdl-grid"}>
                   <div className={"mdl-cell mdl-cell--2-col"}>
           
                   </div>
                   <div className={"mdl-cell mdl-cell--4-col"}>
                       <CreateEvent />
                   </div>
                   <div className={"mdl-cell mdl-cell--4-col"}>
                <EventList />
                   </div>
                   <div className={"mdl-cell mdl-cell--2-col"}>
                       <ConnectionsCard />
                   </div>
               </div>;
    }
}
