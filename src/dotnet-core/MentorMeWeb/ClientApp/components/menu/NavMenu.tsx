﻿import * as React from 'react'
import { Link } from 'react-router'
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import {
    blue300,
    indigo900,
    orange200,
    deepOrange300,
    pink400,
    purple500,
    } from 'material-ui/styles/colors';

const style = { margin: 5 };
export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return <header className={"mdl-layout__header"}>
            <div className={"mdl-layout__header-row"}>
                <Link to={'/'} activeClassName='active'>
                    <span className={"mdl-layout-title"}>Title</span>
                </Link>
                    
                <div className={"mdl-layout-spacer"}></div>
                <Link to={'/login'} activeClassName='active'>
                    <span className='glyphicon glyphicon-home'>Login</span> 
                </Link>
                       <nav className={"mdl-navigation mdl-layout--large-screen-only"}>
                    <div className={"material-icons mdl-badge mdl-badge--overlap"}>notifications</div>
                    <Link to={"/profile"} activeClassName="active">
                        <Avatar
                            color={deepOrange300}
                            backgroundColor={purple500}
                            size={30}
                            style={style}
                        >
                            A
                        </Avatar>
                        </Link>
                           
                       </nav>
                   </div>
               </header>;
    }
}