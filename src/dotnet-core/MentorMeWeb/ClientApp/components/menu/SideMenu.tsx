﻿import * as React from 'react'
import { Link } from 'react-router'

export class SideMenu extends React.Component<{},{}> {
    public render() {
        return <div className={"mdl-layout__drawer"}>
                   <span className={"mdl-layout-title"}>Sec title</span>
                   <nav className={"mdl-navigation"}>
                <a className={"mdl-navigation__link"} href=""> Challenges </a>
                       <Link to={'/events'} activeClassName='active'>
                           <span className={"mdl-navigation__link"}> Events </span>
                       </Link>
                       
                   </nav>
               </div>;
    }
}