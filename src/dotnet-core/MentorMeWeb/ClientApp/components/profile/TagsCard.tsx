﻿import * as React from 'react';

export default class TagsCard extends React.Component<{}, {}> {
    public render() {
        return <div style={{ width: "100%" }} className={"mdl-card mdl-shadow--2dp"}>
                   <div className={"mdl-card__title"}>
                       <h4 className={"mdl-card__title-text"}>Skills</h4>
                   </div>
                   <div className={"mdl-card__supporting-text"}>
                       <span className={"mdl-chip mdl-chip--contact"}>
                           <span className={"mdl-chip__contact mdl-color--teal mdl-color-text--white"}>22</span>
                           <span className={"mdl-chip__text"}>C#</span>
                       </span>
                       <span className={"mdl-chip mdl-chip--contact"}>
                           <span className={"mdl-chip__contact mdl-color--teal mdl-color-text--white"}>34</span>
                           <span className={"mdl-chip__text"}>ReactJS</span>
                       </span>
                   </div>
                   <div className={"mdl-card__menu"}>
                       <button className={"mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect"}>
                           <i className={"material-icons"}>share </i>
                       </button>
                   </div>
               </div>;
    }
}
