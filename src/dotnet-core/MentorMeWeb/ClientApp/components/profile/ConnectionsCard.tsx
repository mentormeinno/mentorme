﻿import * as React from 'react';

export default class ConnectionsCard extends React.Component<{}, {}> {
    public render() {
        return <div style={{ width: "100%" }} className={"mdl-card mdl-shadow--2dp"}>
                   <div className={"mdl-card__title"}>
                       <h2 className={"mdl-card__title-text"}>Connections</h2>
                   </div>
                   <div className={"mdl-card__supporting-text"}>
                       You have no badges yet.
                   </div>
                   <div className={"mdl-card__menu"}>
                       <button className={"mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect"}>
                           <i className={"material-icons"}>share </i>
                       </button>
                   </div>
               </div>;
    }
}
