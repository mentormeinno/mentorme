import * as React from 'react';
import { Router, Route, HistoryBase } from 'react-router';
import { Layout } from './components/Layout';
import Profile from './components/Profile';
import FetchData from './components/FetchData';
import Counter from './components/Counter';
import Events from "./components/Events";
import Login from "./components/Login";
import Home from "./components/Home";

export default <Route component={Layout}>
    <Route path="/" components={{body: Home}} />
    <Route path='/profile' components={{ body: Profile }} />
    <Route path='/login' components={{ body: Login }} />
    <Route path='/counter' components={{ body: Counter }} />
    <Route path='/events' components={{ body: Events }} />
    <Route path='/fetchdata' components={{ body: FetchData }}>
        <Route path='(:startDateIndex)' /> { /* Optional route segment that does not affect NavMenu highlighting */ }
    </Route>
</Route>;

// Enable Hot Module Replacement (HMR)
if (module.hot) {
    module.hot.accept();
}
