﻿import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface TagsState {
    tags: string[],
    isLoading: boolean;
}
// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestTagsAction {
    type: 'REQUEST_TAGS';
}

interface ReceiveTagsAction {
    type: 'RECEIVE_TAGS';
    tags: string[];
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestTagsAction | ReceiveTagsAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestTags: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        let fetchTask = fetch('http://mentor-me.azurewebsites.net/lookup/tags',
                {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                dispatch({ type: 'RECEIVE_TAGS', tags: data });
            });

        addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
        dispatch({ type: 'REQUEST_TAGS' });
    }
}

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: TagsState = { tags: [],isLoading:false};

export const reducer: (state: TagsState, action: KnownAction) => { isLoading: boolean } | { tags: string[];isLoading: boolean } = (state: TagsState, action: KnownAction) => {
    switch (action.type) {
        case 'REQUEST_TAGS':
        return {
            isLoading: true
        };
    case 'RECEIVE_TAGS':
        // Only accept the incoming data if it matches the most recent request. This ensures we correctly
        // handle out-of-order responses.
        {
            return {
                tags: action.tags,
                isLoading: false
            };
        }
 
    default:
        // The following line guarantees that every action in the KnownAction union has been covered by a case above
        const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
};
