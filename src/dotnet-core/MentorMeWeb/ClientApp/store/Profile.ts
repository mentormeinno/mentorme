﻿import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface ProfileState {
    isLoading: boolean;
    email: string;
    reputation: number;
    tags: string[];
    userid: string;
}


// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestProfileAction {
    type: 'REQUEST_PROFILE',
    userid: string
}

interface ReceiveProfileAction {
    type: 'RECEIVE_PROFILE',
    email: string,
    reputation: number,
    tags: string[]
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestProfileAction | ReceiveProfileAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestProfile: (userid: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        let fetctTask = fetch('http://mentor-me.azurewebsites.net/profile/' + userid)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                dispatch({
                    type: 'RECEIVE_PROFILE',
                    email: data.Email,
                    reputation: data.Reputation,
                    userid: userid,
                    tags: data.TagsSortedByReputation
                });
            });
        addTask(fetctTask);
        dispatch({ type: 'REQUEST_PROFILE', userid: userid });
        /*// Only load data if it's something we don't already have (and are not already loading)
        if (startDateIndex !== getState().weatherForecasts.startDateIndex) {
            let fetchTask = fetch(`/api/SampleData/WeatherForecasts?startDateIndex=${ startDateIndex }`)
                .then(response => response.json() as Promise<WeatherForecast[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_WEATHER_FORECASTS', startDateIndex: startDateIndex, forecasts: data });
                });

            addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
            dispatch({ type: 'REQUEST_WEATHER_FORECASTS', startDateIndex: startDateIndex });
        }*/
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: ProfileState = { isLoading: false,email:"",tags:[] as string[],reputation:0,userid:"" };

export const reducer: (state: ProfileState, action: KnownAction) => { userid: string;isLoading: boolean } |
                                                                    {
                                                                        email: string;
                                                                        reputation: number;
                                                                        tags: string[];
                                                                        isLoading: boolean
                                                                    } |
                                                                    ProfileState = (state: ProfileState, action: KnownAction) => {
    switch (action.type) {
    case 'REQUEST_PROFILE':
        return {
            userid: state.userid,
            isLoading: true
        };
    case 'RECEIVE_PROFILE':
        // Only accept the incoming data if it matches the most recent request. This ensures we correctly
        // handle out-of-order responses.
       
            return {
                email: action.email,
                reputation: action.reputation,
                tags: action.tags,
                isLoading: false
            };
    default:
        // The following line guarantees that every action in the KnownAction union has been covered by a case above
        const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
};
