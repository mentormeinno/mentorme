﻿import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface SaveEventState {
    isLoading: boolean,
    evtype: string,
    name: string,
    tags: Array<string>,
    eventid: string,
    typeindex: number;
}

export interface EventsState {
    
}

interface RequestGetEvents {
    type: 'REQUEST_GET_EVENTS',
    userid: string;
}
interface ReceiveEvents {
    type: 'RECEIVE_EVENTS',
    events: Events[]
}

export interface Events {
    name: string,
    type: string;
}
// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.
interface RequestSaveEventAction {
    type: 'REQUEST_SAVE_EVENT',
    evtype: string,
    name: string,
    tags: Array<string>,
    presenterid: string;
}

interface ReceiveSaveEventAction {
    type: 'RECEIVE_SAVE_EVENT',
    eventid: string;
}


// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestSaveEventAction | ReceiveSaveEventAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).
/*declare var Headers: any;
var headers = new Headers();
headers.append("Content-Type", "application/json");*/
export const actionCreators = {
    requestSaveEvent: (evtype: string,
        name: string,
        tags: Array<string>,
        presenterid: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        
    }
}

//export const actionCreators = {
//    requestLogin: (email: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
//        if (getState().login.isLoggedIn) {
//            return;
//        }
//        let fetchLogin = fetch('http://mentor-me.azurewebsites.net/profile',
//                {
//                    method: "POST",
//                    body: JSON.stringify({
//                        "Email": email
//                    }),
//                    headers: {
//                        "Content-Type": "application/x-www-form-urlencoded"
//       /*             }
//                })
//            .then(response => response.json())
//            .then(data => {
//                console.log(data);
//                dispatch({ type: 'RECEIVE_LOGIN', email: email, id: data });
//            });
//        addTask(fetchLogin);
//        dispatch({ type: 'REQUEST_LOGIN', email: email });*/
//    }
//}

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.
const unloadedState: SaveEventState = { isLoading: false, evtype: "", name: "", tags: [] as Array<string>, eventid: "", typeindex:1};
/*const unloadedState: WeatherForecastsState = { startDateIndex: null, forecasts: [], isLoading: false };*/

export const reducer: (state: SaveEventState, action: KnownAction) =>
    { isLoading: boolean;eventid: string;evtype: string;name: string;tags: string[] } = (state: SaveEventState, action: KnownAction) => {
    switch (action.type) {
        case 'REQUEST_SAVE_EVENT':
        return {
            isLoading: true,
            evtype: state.evtype,
            name: state.name,
            tags: state.tags,
            eventid: ""
        };
    case 'RECEIVE_SAVE_EVENT':
        return {
            isLoading: false,
            eventid: action.eventid,
            evtype: state.evtype,
            name: state.name,
            tags: state.tags
        };
    default:
        // The following line guarantees that every action in the KnownAction union has been covered by a case above
        const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
};
