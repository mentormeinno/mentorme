﻿import { fetch, addTask } from 'domain-task';
import { Action, Reducer, ActionCreator } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface LoginState {
    isLoading: boolean;
    isLoggedIn: boolean;
    email: string;
    id: string;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestLoginAction {
    type: 'REQUEST_LOGIN',
    email: string;
}

interface ReceiveLoginAction {
    type: 'RECEIVE_LOGIN',
    email: string,
    id: string;
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestLoginAction | ReceiveLoginAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).
/*declare var Headers: any;
var headers = new Headers();
headers.append("Content-Type", "application/json");*/
export const actionCreators = {
    requestLogin: (email: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        if (getState().login.isLoggedIn) {
            return;
        }
        let fetchLogin = fetch('http://mentor-me.azurewebsites.net/profile',
                {
                    method: "POST",
                    body: JSON.stringify({
                        "Email": email
                    }),
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                dispatch({ type: 'RECEIVE_LOGIN', email: email, id: data });
            });
        addTask(fetchLogin);
        dispatch({ type: 'REQUEST_LOGIN', email: email});
    }   
}

/*
export const actionCreators = {
    requestWeatherForecasts: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        if (startDateIndex !== getState().weatherForecasts.startDateIndex) {
            let fetchTask = fetch(`/api/SampleData/WeatherForecasts?startDateIndex=${startDateIndex}`)
                .then(response => response.json() as Promise<WeatherForecast[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_WEATHER_FORECASTS', startDateIndex: startDateIndex, forecasts: data });
                });

            addTask(fetchTask); // Ensure server-side prerendering waits for this to complete
            dispatch({ type: 'REQUEST_WEATHER_FORECASTS', startDateIndex: startDateIndex });
        }
    }
};
*/

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.
const unloadedState: LoginState = { isLoggedIn: false, email: "", id: "", isLoading: false };
/*const unloadedState: WeatherForecastsState = { startDateIndex: null, forecasts: [], isLoading: false };*/

export const reducer: Reducer<LoginState> = (state: LoginState, action: KnownAction) => {
    switch (action.type) {
        case 'REQUEST_LOGIN':
            return{
                isLoading: true,
                email: state.email,
                isLoggedIn: state.isLoggedIn,
                id: state.id
            };
        case 'RECEIVE_LOGIN':
            return {
                isLoading: false,
                email: action.email,
                id: action.id,
                isLoggedIn: true
            };
    default:
        // The following line guarantees that every action in the KnownAction union has been covered by a case above
        const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
};
