﻿namespace MentorMe.App.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Newtonsoft.Json;

    public class MentorController : Controller
    {
        public ActionResult Index(string id)
        {
            ViewBag.MentorId = id;
            return View();
        }

        public async Task<ActionResult> ReviewChallenge(string mentorId, string menteeId)
        {
            ViewBag.MentorId = mentorId;
            ViewBag.MenteeId = menteeId;

            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync($"http://mentor-me.azurewebsites.net/profile/{mentorId}");

            var profile = JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync());

            try
            {
                var requestChallanges =
                    (List<dynamic>)(profile).Mentor.RequestedChallengesFromMentees.ToObject<List<dynamic>>();

                List<dynamic> requestChallangesFromMentee =
                    requestChallanges.Where(x => x.MenteeId == menteeId).ToList();

                foreach (var challenge in requestChallangesFromMentee)
                {
                    
                }

                return View(requestChallangesFromMentee);
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}