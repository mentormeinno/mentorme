﻿namespace MentorMe.App.Controllers
{
    using System.Web.Mvc;

    public class CreateChallengeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }

    public class AddChallengeModel
    {
        public string MentorId { get; set; }

        public string ChallengeText { get; set; }
    }
}