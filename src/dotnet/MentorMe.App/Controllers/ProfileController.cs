﻿namespace MentorMe.App.Controllers
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Newtonsoft.Json;

    public class ProfileController : Controller
    {
        public async Task<ActionResult> Other(string profileId)
        {
            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync($"http://mentor-me.azurewebsites.net/profile/{profileId}");
            var profile = JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync());
            var model = new Profile
                            {
                                ProfileEmail = profile.Email,
                                ProfileId = profileId,
                                CurrentProfileId = Session["CurrentProfileId"]?.ToString(),
                                CurrentProfileEmail = Session["CurrentProfileEmail"]?.ToString(),
                                Reputation = (decimal)profile.Reputation,
                                Tags = profile.TagsSortedByReputation.ToObject<Dictionary<string, decimal>>(),
                            };

            Session["ProfileId"] = model.ProfileId;
            Session["ProfileEmail"] = model.ProfileEmail;

            return View("Index", model);
        }

        public async Task<ActionResult> Index()
        {
            var profileId = Session["ProfileId"];

            if (string.IsNullOrEmpty(profileId?.ToString()))
            {
                return RedirectToAction("Create");
            }

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync($"http://mentor-me.azurewebsites.net/profile/{profileId}");
            var profile = JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync());
            var model = new Profile
                            {
                                ProfileEmail = profile.Email,
                                ProfileId = profileId.ToString(),
                                CurrentProfileId = profileId.ToString(),
                                CurrentProfileEmail = profile.Email,
                                Reputation = (decimal)profile.Reputation,
                                Tags = profile.TagsSortedByReputation.ToObject<Dictionary<string, decimal>>(),
                            };

            Session["CurrentProfileId"] = model.ProfileId;
            Session["CurrentProfileEmail"] = model.ProfileEmail;

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register()
        {
            var httpClient = new HttpClient();

            var foo = new { Email = Request.Form["Email"] };
            var httpContent = foo.ToHttpContent();

            var response = await httpClient.PostAsync("http://mentor-me.azurewebsites.net/profile", httpContent);

            var profileId = JsonConvert.DeserializeObject<string>(await response.Content.ReadAsStringAsync());
            Session["ProfileId"] = profileId;

            return RedirectToAction("Index");
        }
    }

    public class Profile
    {
        public string ProfileId { get; set; }

        public string ProfileEmail { get; set; }

        public decimal Reputation { get; set; }

        public Dictionary<string, decimal> Tags { get; set; }

        public string CurrentProfileId { get; set; }

        public string CurrentProfileEmail { get; set; }
    }
}