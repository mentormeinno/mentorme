﻿namespace MentorMe.App.Controllers
{
    using System.Web.Mvc;

    public class EventsController:Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Feed()
        {
            return View();
        }
    }
}