﻿namespace MentorMe.App.Controllers
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Newtonsoft.Json;

    public class FeedbackController : Controller
    {
        public async Task<ActionResult> Index(string participantId, string eventId)
        {
            var client1 = new HttpClient();

            var eventResponse = await client1.GetAsync($"http://mentor-me.azurewebsites.net/event/{eventId}");
            var @event = JsonConvert.DeserializeObject<dynamic>(await eventResponse.Content.ReadAsStringAsync());

            var presenterResponse =
                await client1.GetAsync($"http://mentor-me.azurewebsites.net/profile/{@event.PresenterId}");
            var @presenter = JsonConvert.DeserializeObject<dynamic>(await presenterResponse.Content.ReadAsStringAsync());

            var feedbackModel = new FeedbackModel
                                    {
                                        Presenter = @presenter.Email,
                                        EventName = @event.Name,
                                        FromProfileId = participantId,
                                        EventId = eventId,
                                        Tags = @event.Tags.ToObject<List<string>>()
                                    };

            return View(feedbackModel);
        }
    }

    public class FeedbackModel
    {
        public List<string> Tags { get; set; }

        public string Presenter { get; set; }

        public string EventName { get; set; }

        public string FromProfileId { get; set; }

        public string EventId { get; set; }
    }
}