namespace MentorMe.App.Controllers
{
    using System.Net.Http;
    using System.Text;

    using Newtonsoft.Json;

    public static class RequestExtensions
    {
        public static HttpContent ToHttpContent(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}