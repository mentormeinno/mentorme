namespace MentorMe.Configuration
{
    using System.Configuration;

    public class FileApplicationConfigProvider : IAppConfigProvider
    {
        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}