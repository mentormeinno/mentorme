﻿namespace MentorMe.Configuration
{
    public interface IAppConfigProvider
    {
        string Get(string key);
    }
}