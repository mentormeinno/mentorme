﻿namespace MentorMe.Configuration
{
    using Autofac;

    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileApplicationConfigProvider>().As<IAppConfigProvider>().SingleInstance();
        }
    }
}