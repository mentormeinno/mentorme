namespace MentorMe.WebApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MediatR;

    using MentorMe.WebApi.Features.Challenge;
    using MentorMe.WebApi.Features.Profile;
    using MentorMe.WebApi.Features.Profile.MentorshipSpace;
    using MentorMe.WebApi.Features.Profile.Notification;

    [RoutePrefix("profile")]
    public class ProfileController : ApiController
    {
        private readonly ProfileRepository profileRepository;

        private readonly IMediator mediator;

        private readonly ChallengeRepository challengeRepository;

        public ProfileController(
            ProfileRepository profileRepository,
            IMediator mediator,
            ChallengeRepository challengeRepository)
        {
            this.mediator = mediator;
            this.challengeRepository = challengeRepository;
            this.profileRepository = profileRepository;
        }

        [HttpGet]
        [Route("{id}/challenges/{menteeId}")]
        public HttpResponseMessage GetAvailableChallengesFromMentor(string id, string menteeId)
        {
            var mentorProfile = profileRepository.GetById(id);
            List<string> allMentorChallengeIds = mentorProfile.Mentor.ChallengeIds;
            List<string> challengesAlreadyTackledByMentee =
                mentorProfile.Mentor.RequestedChallengesFromMentees.Where(rc => rc.MenteeId == menteeId)
                    .Select(r => r.ChallengeId)
                    .ToList();
            List<string> availableChallengesIdsForMentee =
                allMentorChallengeIds.Except(challengesAlreadyTackledByMentee).ToList();

            List<Challenge> challengesByIds =
                challengeRepository.GetChallengesByIds(availableChallengesIdsForMentee).ToList();
            var challenges =
                challengesByIds.Select(ch => new { ch.Id, ch.Type, ch.Tags, ch.AddedBy, ch.ChallengeText, ch.MinimumLevel });

            return Request.CreateResponse(
                HttpStatusCode.OK,
                new { Challenges = challenges, mentorProfile.Email, mentorProfile.Reputation });
        }

        [HttpGet]
        [Route("{id}")]
        public HttpResponseMessage Get(string id)
        {
            var profile = profileRepository.GetById(id);
            var badgeStash =
                profile.BadgeStash.Select(bs => new { bs.Name, bs.Level, bs.Type, bs.TypeDescription, bs.Value });

            return profile == null
                       ? Request.CreateResponse(HttpStatusCode.NotFound)
                       : Request.CreateResponse(
                           HttpStatusCode.OK,
                           new
                               {
                                   profile.Mentor.ChallengeIds,
                                   profile.Mentee.ChallengeTakenUpIds,
                                   profile.Mentee.ResolvedChallengeIds,
                                   profile.Email,
                                   profile.Reputation,
                                   TagsSortedByReputation = profile.GetTagsSortedByReputation(),
                                   profile.Mentee,
                                   profile.Mentor,
                                   BadgeStash = badgeStash
                               });
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post([FromBody] CreateProfileRequest request)
        {
            var existingProfile = profileRepository.GetProfileByEmail(request.Email);
            if (existingProfile != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, existingProfile.Id);
            }

            var profile = profileRepository.Add(new Profile { Email = request.Email });

            return Request.CreateResponse(HttpStatusCode.Created, profile.Id);
        }

        [HttpGet]
        [Route("{id}/search/{tag}/{page}")]
        public HttpResponseMessage GetBy(string id, string tag, int page)
        {
            IEnumerable<Profile> mentors = profileRepository.GetMentorsByTagForPage(page, tag, id);

            var mentorsForPage =
                mentors.Select(
                    m =>
                        new
                            {
                                m.Id,
                                Tags = m.GetTagsSortedByReputation(),
                                m.Email,
                                m.Mentee,
                                m.Mentor,
                                m.Reputation,
                                BadgeStash = m.BadgeStash.Select(bs => new { bs.Name, bs.Level, bs.Type, bs.TypeDescription, bs.Value })
                        });

            return Request.CreateResponse(HttpStatusCode.OK, mentorsForPage);
        }

        [HttpGet]
        [Route("{id}/notifications")]
        public HttpResponseMessage GetNotifications(string id)
        {
            IEnumerable<ResponseForAllNotifications> response =
                mediator.Send(new RequestForAllNotifications { ProfileId = id });

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("{id}/sugesstions")]
        public HttpResponseMessage GetSuggestedProfiles(string id)
        {
            var profile = profileRepository.GetById(id);
            var tagsCollection = new List<string>(profile.Tags.Keys);

            IEnumerable<Profile> profilesWithHighRankOn = profileRepository.GetProfilesWithHighRankOn(
                tagsCollection,
                id);

            return Request.CreateResponse(
                HttpStatusCode.OK,
                profilesWithHighRankOn.Select(
                    pr => new { pr.Mentor, pr.Mentee, pr.Email, pr.Tags, pr.Reputation, pr.Id, pr.BadgeStash }));
        }

        [HttpPost]
        [Route("{menteeId}/mentorship/request")]
        public async Task<HttpResponseMessage> RequestForMentorship(
            [FromBody] MentorshipLinkRequest mentorshipLinkRequest,
            string menteeId)
        {
            await
                mediator.PublishAsync(
                    new MentorshipAddEvent
                        {
                            MenteeId = menteeId,
                            MentorId = mentorshipLinkRequest.MentorId,
                            Tag = mentorshipLinkRequest.Tag,
                        });
            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPost]
        [Route("{mentorId}/mentorship/response")]
        public async Task<HttpResponseMessage> ResponseForMentorship(
            [FromBody] MentorshipLinkResponse mentorshipLinkResponse,
            string mentorId)
        {
            await
                mediator.PublishAsync(
                    new MentorshipUpdateEvent
                        {
                            MenteeId = mentorshipLinkResponse.MenteeId,
                            MentorId = mentorId,
                            Status = (MentorshipStatus)mentorshipLinkResponse.Status,
                            RejectionReason = mentorshipLinkResponse.RejectionReason
                        });

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpGet]
        [Route("{mentorId}/mentorship/menteespending")]
        public HttpResponseMessage GetMyRequestedMentees(string mentorId)
        {
            var mentorProfile = profileRepository.GetById(mentorId);
            IEnumerable<Mentorship> menteeRequests =
                mentorProfile.Mentor.MyMentees.FindAll(x => x.Status == MentorshipStatus.Requested);

            return Request.CreateResponse(HttpStatusCode.OK, menteeRequests);
        }

        [HttpGet]
        [Route("{menteeId}/mentorship/mentorspending")]
        public HttpResponseMessage GetMentorsRequestedByMe(string menteeId)
        {
            var menteeProfile = profileRepository.GetById(menteeId);
            IEnumerable<Mentorship> mentorRequested =
                menteeProfile.Mentee.MyMentors.FindAll(x => x.Status == MentorshipStatus.Requested);

            return Request.CreateResponse(HttpStatusCode.OK, mentorRequested);
        }

        public class CreateProfileRequest
        {
            public string Email { get; set; }
        }
    }

    public class MentorshipLinkResponse
    {
        public string MenteeId { get; set; }

        public int Status { get; set; }

        public string RejectionReason { get; set; }
    }

    public class MentorshipLinkRequest
    {
        public string MentorId { get; set; }

        public string Tag { get; set; }
    }
}