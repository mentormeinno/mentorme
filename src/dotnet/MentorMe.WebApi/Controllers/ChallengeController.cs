﻿namespace MentorMe.WebApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MediatR;

    using MentorMe.WebApi.Features.Badge;
    using MentorMe.WebApi.Features.Challenge;
    using MentorMe.WebApi.Features.Challenge.Response;
    using MentorMe.WebApi.Features.Challenge.TakeUp;
    using MentorMe.WebApi.Features.Profile;

    [RoutePrefix("challenge")]
    public class ChallengeController : ApiController
    {
        private readonly ChallengeRepository challengeRepository;

        private readonly ProfileRepository profileRepository;

        private readonly IMediator mediator;

        public ChallengeController(
            ChallengeRepository challengeRepository,
            ProfileRepository profileRepository,
            IMediator mediator)
        {
            this.challengeRepository = challengeRepository;
            this.profileRepository = profileRepository;
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("{id}")]
        public HttpResponseMessage Get(string id)
        {
            var response = challengeRepository.GetById(id);

            return Request.CreateResponse(
                HttpStatusCode.OK,
                new { response.AddedBy, response.ChallengeText, response.Type, response.Tags });
        }

        public class GetChallengesByManyIds
        {
            public List<string> ids { get; set; }
        }

        [HttpPost]
        [Route("retrieve/specific")]
        public HttpResponseMessage GetChallenges([FromBody]GetChallengesByManyIds obj)
        {
            if(obj == null)
                return Request.CreateResponse(
               HttpStatusCode.NotModified,
               new {});
            IList<Challenge> response = new List<Challenge>();
            if (obj.ids != null)
                response = challengeRepository
                .Where(c => obj.ids.Contains(c.Id))
                .ToList();

            var list =
                response.Select(r => 
                new { r.Id, r.ChallengeText, r.MinimumLevel })
                .ToList();


            return Request.CreateResponse(
                HttpStatusCode.OK,
                list);
        }

        [HttpPost]
        [Route("{mentorId}")]
        public async Task<HttpResponseMessage> Post(string mentorId, CreateChallengeRequest request)
        {
            var newChallenge = challengeRepository.Add(
                new Challenge
                {
                    Tags = request.Tags,
                    ChallengeText = request.ChallengeText,
                    Type = request.Type,
                    AddedBy = request.AddedByMentor,
                    MinimumLevel = request.MinimumLevel,
                    Difficulty = request.Difficulty
                });

            var mentorProfile = profileRepository.GetById(mentorId);
            mentorProfile.Mentor.ChallengeIds.Add(newChallenge.Id);
            profileRepository.Update(mentorProfile);

            await mediator.PublishAsync(
                    new BadgeActivateEvent
                    {
                        ProfileId = mentorId,
                        BadgeType = BadgeType.ShareAChallenge
                    });

            return Request.CreateResponse(HttpStatusCode.Created, newChallenge.Id);
        }

        [HttpPost]
        [Route("{challengeId}/takeup")]
        public async Task<HttpResponseMessage> TakeUpChallenge(string challengeId, string menteeId, string mentorId)
        {
            await
                mediator.PublishAsync(
                    new TakeupChallengeEvent { ChallengeId = challengeId, MenteeId = menteeId, MentorId = mentorId });

            await mediator.PublishAsync(
                    new BadgeActivateEvent
                    {
                        ProfileId = menteeId,
                        BadgeType = BadgeType.TakeUpChallenge
                    });

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPost]
        [Route("{challengeId}/submitforreview")]
        public async Task<HttpResponseMessage> SubmitChallengeForReview(string challengeId, ResponseToChallenge response)
        {
            await mediator.PublishAsync(
               new SubmitChallengeResponseForReviewEvent
               {
                   ChallengeId = challengeId,
                   Reply = response.Reply,
                   MenteeId = response.MenteeId
               });

            return Request.CreateResponse(HttpStatusCode.Created);
        }
        [HttpPost]
        [Route("{challengeId}/acceptchallengesubmission")]
        public async Task<HttpResponseMessage> AcceptChallengeSubmission(string challengeId, string menteeId)
        {
            await mediator.PublishAsync(
                new AcceptChallengeSubmissionEvent {
                        ChallengeId = challengeId,
                        MenteeId=menteeId
                });

            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }

   

    public class CreateChallengeRequest
    {
        public string ChallengeText { get; set; }

        public string Type { get; set; }

        public string AddedByMentor { get; set; }

        public List<string> Tags { get; set; }

        public int MinimumLevel { get; set; }

        public int Difficulty { get; set; }
    }

    public class ResponseToChallenge
    {
        public string Reply { get; set; }

        public string MenteeId { get; set; }
    }
}