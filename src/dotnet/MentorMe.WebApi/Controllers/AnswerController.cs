﻿using System.Web.Http;

namespace MentorMe.WebApi.Controllers
{
    using System;
    using System.Net;
    using System.Net.Http;

    using Features.Challenge;

    [RoutePrefix("answer")]
    public class AnswerController : ApiController
    {
        private readonly AnswerRepository answerRepository;

        public AnswerController(AnswerRepository answerRepository)
        {
            this.answerRepository = answerRepository;
        }

        [HttpGet]
        [Route("{id}")]
        public HttpResponseMessage Get(string id)
        {
            var response = answerRepository.GetById(id);

            return Request.CreateResponse(
                HttpStatusCode.OK,
                new { response.AnswerId, response.ChallengeId, response.AnswerText, response.AnsweredBy });
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post([FromBody] AddAnswareToChallenge answer)
        {
            var ev = answerRepository.Add(
                new Answer
                {
                    AnswerId = answer.AnswerId,
                    ChallengeId = answer.ChallengeId,
                    AnswerText = answer.AnswerText,
                    AnsweredBy = answer.AnsweredBy
                });

            return Request.CreateResponse(HttpStatusCode.Created, ev.Id);
        }
    }

    public class AddAnswareToChallenge
    {
        public int AnswerId { get; set; }

        public int ChallengeId { get; set; }

        public string AnswerText { get; set; }

        public Guid AnsweredBy { get; set; }
    }
}