﻿namespace MentorMe.WebApi.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MediatR;

    using MentorMe.WatsonService.Wrapper;
    using MentorMe.WebApi.Features.Event.Feedback;
    using MentorMe.WebApi.Features.Event.Query;
    using MentorMe.WebApi.Features.Profile.Reputation.Add;

    [RoutePrefix("feedback")]
    public class FeedbackController : ApiController
    {
        private readonly IMediator mediator;

        private readonly INaturalLanguageProcessor languageProcessor;

        public FeedbackController(IMediator mediator, INaturalLanguageProcessor languageProcessor)
        {
            this.mediator = mediator;
            this.languageProcessor = languageProcessor;
        }

        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post([FromBody] AddFeedbackToRequest request)
        {
            var feedback = await languageProcessor.Get(request.Text);

            if (IsFeedbackRelevant(feedback))
            {
                var eventDetails = await mediator.SendAsync(new RequestEventDetails { EventId = request.EventId });

                await
                    mediator.PublishAsync(
                        new IncreaseReputationOfPresenter
                            {
                                PresenterId = eventDetails.PresenterId,
                                ParticipantId = request.FromProfileId,
                                Tags = eventDetails.Tags,
                                PresenterFeedback = feedback
                            });

                await
                    mediator.PublishAsync(
                        new IncreaseReputationOfParticipant
                            {
                                PresenterId = eventDetails.PresenterId,
                                ParticipantId = request.FromProfileId,
                                Tags = eventDetails.Tags,
                                Feedback = feedback
                            });
            }

            await mediator.PublishAsync(new AddEmojiToFeedback { Emoji = request.Emoji, EventId = request.EventId });
            
            return Request.CreateResponse(HttpStatusCode.OK, feedback);
        }

        private static bool IsFeedbackRelevant(NlpResponse feedback)
        {
            if (feedback.AngerValue == feedback.DisgustValue && feedback.DisgustValue == feedback.FearValue
                && feedback.FearValue == feedback.JoyValue && feedback.JoyValue == feedback.SadnessValue)
            {
                return false;
            }

            return true;
        }
    }

    public class AddFeedbackToRequest
    {
        public string FromProfileId { get; set; }

        public string EventId { get; set; }

        public string Text { get; set; }

        public string Emoji { get; set; }
    }
}