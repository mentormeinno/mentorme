﻿namespace MentorMe.WebApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MediatR;

    using MentorMe.WebApi.Features.Event;
    using MentorMe.WebApi.Features.Tag;

    [RoutePrefix("event")]
    public class EventController : ApiController
    {
        private readonly EventRepository eventRepository;

        private readonly IMediator mediator;

        public EventController(EventRepository eventRepository, IMediator mediator)
        {
            this.eventRepository = eventRepository;
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("{id}")]
        public HttpResponseMessage Get(string id)
        {
            var response = eventRepository.GetById(id);

            if (response == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(
                HttpStatusCode.OK,
                new
                    {
                        response.Name,
                        response.PresenterId,
                        response.Type,
                        response.EmojiesCount,
                        response.Tags,
                        response.StartTime,
                        response.Location
                    });
        }

        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> Post([FromBody] CreateEventRequest request)
        {
            var ev =
                eventRepository.Add(
                    new Event
                        {
                            Tags = request.Tags,
                            Name = request.Name,
                            Type = request.Type,
                            PresenterId = request.PresenterId,
                            StartTime = request.StartTime,
                            Location = request.Location
                        });

            await mediator.PublishAsync(new TagsAddedEvent { TagsNames = request.Tags });

            return Request.CreateResponse(HttpStatusCode.Created, ev.Id);
        }

        [HttpGet]
        [Route("")]
        public HttpResponseMessage GetAllEvents()
        {
            List<Event> response = eventRepository.GetAllEventsStartingFrom(DateTime.Today).ToList();

            if (response.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            List<object> events = response.Select(MapFrom).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [HttpGet]
        [Route("presenter/{presenterId}")]
        public HttpResponseMessage GetAllEventsFor(string presenterId)
        {
            List<Event> response = eventRepository.GetAllEventsFor(presenterId).ToList();

            if (response.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            List<object> events = response.Select(MapFrom).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        [HttpGet]
        [Route("location/{location}")]
        public HttpResponseMessage GetAllEventsInLocation(string location)
        {
            List<Event> response = eventRepository.GetAllEventsInLocation(location).ToList();

            if (response.Count == 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            List<object> events = response.Select(MapFrom).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, events);
        }

        private static object MapFrom(Event ev)
        {
            return new { ev.Emojies, ev.Id, ev.Name, ev.PresenterId, ev.Tags, ev.Type, ev.Location, ev.StartTime, ev.EmojiesCount };
        }
    }

    public class CreateEventRequest
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public List<string> Tags { get; set; }

        public string PresenterId { get; set; }

        public string Location { get; set; }

        public DateTime StartTime { get; set; }
    }
}