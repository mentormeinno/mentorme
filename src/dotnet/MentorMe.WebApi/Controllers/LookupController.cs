namespace MentorMe.WebApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using MentorMe.WebApi.Features.Badge;
    using MentorMe.WebApi.Features.Tag;

    [RoutePrefix("lookup")]
    public class LookupController : ApiController
    {
        private readonly TagRepository tagRepository;

        public LookupController(TagRepository tagRepository)
        {
            this.tagRepository = tagRepository;
        }

        [HttpGet]
        [Route("tags")]
        public HttpResponseMessage Get()
        {
            List<Tag> response = tagRepository.GetAll();

            var tags = response.Select(r => new { Name = r.Name });
            return Request.CreateResponse(HttpStatusCode.OK, tags);
        }

        [HttpGet]
        [Route("badgeInit")]
        public HttpResponseMessage BadgeInit()
        {
            new BadgeInitializer(new BadgeRepository()).Init();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}