﻿namespace MentorMe.WebApi
{
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.ExceptionHandling;

    using NLog;

    public class ExceptionLogger : IExceptionLogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public virtual Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            Logger.Error(context.Exception, context.Exception.Message);

            if (context.Exception.InnerException != null)
            {
                Logger.Error(context.Exception.InnerException, context.Exception.InnerException.Message);
            }

            return Task.FromResult(0);
        }
    }
}