namespace MentorMe.WebApi
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Http;

    using Autofac;
    using Autofac.Features.Variance;
    using Autofac.Integration.WebApi;

    using MediatR;

    using MentorMe.Configuration;
    using MentorMe.WatsonService.Wrapper;
    using MentorMe.WebApi.Features.Badge;
    using MentorMe.WebApi.Features.Challenge;
    using MentorMe.WebApi.Features.Event;
    using MentorMe.WebApi.Features.Event.Query;
    using MentorMe.WebApi.Features.Profile;
    using MentorMe.WebApi.Features.Tag;

    using NLog;
    using NLog.Config;
    using NLog.Targets;

    using Owin;

    using Module = Autofac.Module;

    internal static class AppBuilderExtensions
    {
        internal static void InitDependencyResolver(this IAppBuilder app, HttpConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule<MediatorModule>();
            builder.RegisterModule<PersistanceModule>();
            builder.RegisterModule<ConfigurationModule>();
            builder.RegisterModule<WatsonServiceModule>();

            configuration.DependencyResolver = new AutofacWebApiDependencyResolver(builder.Build());
        }

        internal static void ConfigureNLog(this IAppBuilder app)
        {
            var config = new LoggingConfiguration();
            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);
            fileTarget.Layout = @"${date:format=HH\:mm\:ss } ${logger} ${message}";
            fileTarget.FileName = $"logs/{DateTime.Today:yyyy-MM-dd}.log";
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, fileTarget));
            LogManager.Configuration = config;
        }

        private class MediatorModule : Module
        {
            protected override void Load(ContainerBuilder builder)
            {
                builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).AsImplementedInterfaces();
                builder.RegisterAssemblyTypes(typeof(RequestEventTagsHandler).Assembly).AsImplementedInterfaces();
                builder.RegisterSource(new ContravariantRegistrationSource());
                builder.Register<SingleInstanceFactory>(
                    ctx =>
                        {
                            var c = ctx.Resolve<IComponentContext>();
                            return t => c.Resolve(t);
                        });
                builder.Register<MultiInstanceFactory>(
                    ctx =>
                        {
                            var c = ctx.Resolve<IComponentContext>();
                            return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
                        });
            }
        }

        private class PersistanceModule : Module
        {
            protected override void Load(ContainerBuilder builder)
            {
                builder.RegisterType<EventRepository>().As<EventRepository>();
                builder.RegisterType<ProfileRepository>().As<ProfileRepository>();
                builder.RegisterType<ChallengeRepository>().As<ChallengeRepository>();
                builder.RegisterType<AnswerRepository>().As<AnswerRepository>();
                builder.RegisterType<TagRepository>().As<TagRepository>();
                builder.RegisterType<BadgeRepository>().As<BadgeRepository>();
            }
        }
    }
}