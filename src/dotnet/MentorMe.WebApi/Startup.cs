﻿using MentorMe.WebApi;

using Microsoft.Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace MentorMe.WebApi
{
    using System;
    using System.Configuration;
    using System.Web.Http;

    using MentorMe.WebApi.Features.Badge;

    using NLog;

    using Owin;

    using Swashbuckle.Application;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var logger = LogManager.GetCurrentClassLogger();

            var configuration = new HttpConfiguration { IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always };
            configuration.EnableCors();

            configuration.EnableSwagger(
                    c =>
                        {
                            var rootUrl = ConfigurationManager.AppSettings["RootUrl"];

                            if (!string.IsNullOrEmpty(rootUrl))
                            {
                                c.RootUrl(req => rootUrl);
                            }

                            c.SingleApiVersion("v1", "MentorMe API");
                        })
                .EnableSwaggerUi();

            WebApiConfig.Register(configuration);

            app.ConfigureNLog();
            app.InitDependencyResolver(configuration);
            app.UseWebApi(configuration);

            //new BadgeInitializer(new BadgeRepository()).Init();

            logger.Info("Application started!");
        }
    }
}