﻿namespace MentorMe.WebApi.Features.Tag
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using MongoRepository;

    public class TagRepository : MongoRepository<Tag>
    {
        public void AddTagIfNew(List<string> tagsNames)
        {
            foreach (var tagName in tagsNames)
            {
                if (TagDoesNotExistIgnoringWhiteSpacesAndCase(tagName))
                {
                    Add(new Tag { Name = tagName });
                }
            }
        }

        public List<Tag> GetAll()
        {
            return Collection.FindAll().ToList();
        }

        private bool TagDoesNotExistIgnoringWhiteSpacesAndCase(string tagName)
        {
            return
                Collection.FindAll()
                    .All(
                        t =>
                            !t.Name.Replace(" ", string.Empty)
                                .Equals(tagName.Replace(" ", string.Empty), StringComparison.OrdinalIgnoreCase));
        }
    }
}