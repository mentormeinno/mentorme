﻿namespace MentorMe.WebApi.Features.Tag
{
    using MongoRepository;

    public class Tag : Entity
    {
        public string Name { get; set; }
    }
}