﻿namespace MentorMe.WebApi.Features.Tag
{
    using System.Threading.Tasks;

    using MediatR;

    public class AddTagHandler : IAsyncNotificationHandler<TagsAddedEvent>
    {
        private readonly TagRepository tagRepository;

        public AddTagHandler(TagRepository tagRepository)
        {
            this.tagRepository = tagRepository;
        }

        public Task Handle(TagsAddedEvent notification)
        {
            tagRepository.AddTagIfNew(notification.TagsNames);
            return Task.FromResult(true);
        }
    }
}