﻿namespace MentorMe.WebApi.Features.Tag
{
    using System.Collections.Generic;

    using MediatR;

    public class TagsAddedEvent : IAsyncNotification
    {
        public List<string> TagsNames { get; set; }
    }
}