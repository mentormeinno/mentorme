﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    using MediatR;

    public class MentorshipAddEvent : IAsyncNotification
    {
        public string MentorId { get; set; }
        public string MenteeId { get; set; }
        public string Tag { get; set; }
    }
}