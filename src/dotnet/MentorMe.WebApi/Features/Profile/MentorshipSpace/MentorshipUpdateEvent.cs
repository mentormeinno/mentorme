﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    using MediatR;

    public class MentorshipUpdateEvent : IAsyncNotification
    {
        public string MentorId { get; set; }
        public string MenteeId { get; set; }
        public MentorshipStatus Status { get; set; }

        public string RejectionReason { get; set; }
    }
}