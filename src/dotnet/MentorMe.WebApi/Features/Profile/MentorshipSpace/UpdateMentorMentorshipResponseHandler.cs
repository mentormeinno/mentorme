﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    using System.Threading.Tasks;

    using MediatR;

    using MentorMe.WebApi.Features.Badge;
    using MentorMe.WebApi.Features.Profile.Extensions;

    public class UpdateMentorMentorshipResponseHandler : IAsyncNotificationHandler<MentorshipUpdateEvent>
    {
        private readonly ProfileRepository profileRepository;

        private readonly IMediator mediator;

        public UpdateMentorMentorshipResponseHandler(ProfileRepository profileRepository, IMediator mediator)
        {
            this.profileRepository = profileRepository;
            this.mediator = mediator;
        }

        public async Task Handle(MentorshipUpdateEvent notification)
        {
            var mentorProfile = profileRepository.GetById(notification.MentorId);

            mentorProfile.UpdateMentorshipResponseForMentor(notification.MenteeId, notification.Status,notification.RejectionReason);

            profileRepository.Update(mentorProfile);

            await EnableBadges(notification);
        }

        private async Task EnableBadges(MentorshipUpdateEvent notification)
        {
            if (notification.Status == MentorshipStatus.Accepted)
            {
                await mediator.PublishAsync(
                    new BadgeActivateEvent
                        {
                            ProfileId = notification.MentorId,
                            BadgeType = BadgeType.FoundAMentee
                        });

            }
        }
    }
}