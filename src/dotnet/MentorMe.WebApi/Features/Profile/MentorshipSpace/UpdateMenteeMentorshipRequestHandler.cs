﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    using System.Threading.Tasks;

    using MediatR;

    using Extensions;

    public class UpdateMenteeRequestForMentorshipHandler : IAsyncNotificationHandler<MentorshipAddEvent>
    {
        private readonly ProfileRepository profileRepository;

        public UpdateMenteeRequestForMentorshipHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(MentorshipAddEvent notification)
        {
            var menteeProfile = profileRepository.GetById(notification.MenteeId);

            menteeProfile.AddMentorshipRequestForMentee(notification.MentorId, notification.Tag);

            profileRepository.Update(menteeProfile);

            return Task.FromResult(true);
        }
    }
}