﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    public class Mentorship
    {
        public string MenteeId { get; set; }

        public string MentorId { get; set; }

        public string SearchedTag { get; set; }

        public MentorshipStatus Status { get; set; }

        public string RejectionReason { get; set; }
    }
}