﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    using System.Threading.Tasks;

    using MediatR;

    using Extensions;
    
    public class UpdateMentorRequestForMentorshipHandler : IAsyncNotificationHandler<MentorshipAddEvent>
    {
        private readonly ProfileRepository profileRepository;

        public UpdateMentorRequestForMentorshipHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(MentorshipAddEvent notification)
        {
            var mentorProfile = profileRepository.GetById(notification.MentorId);

            mentorProfile.AddMentorshipRequestForMentor(notification.MenteeId, notification.Tag);

            profileRepository.Update(mentorProfile);

            return Task.FromResult(true);
        }
    }
}