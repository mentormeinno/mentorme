﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    using System.Threading.Tasks;

    using MediatR;

    using MentorMe.WebApi.Features.Badge;
    using MentorMe.WebApi.Features.Profile.Extensions;

    public class UpdateMenteeResponseForMentorshipHandler : IAsyncNotificationHandler<MentorshipUpdateEvent>
    {
        private readonly ProfileRepository profileRepository;

        private readonly IMediator mediator;

        public UpdateMenteeResponseForMentorshipHandler(ProfileRepository profileRepository, IMediator mediator)
        {
            this.profileRepository = profileRepository;
            this.mediator = mediator;
        }

        public async Task Handle(MentorshipUpdateEvent notification)
        {
            var menteeProfile = profileRepository.GetById(notification.MenteeId);

            menteeProfile.UpdateMentorshipResponseForMentee(notification.MentorId, notification.Status, notification.RejectionReason);

            profileRepository.Update(menteeProfile);

            await EnableBadges(notification);
        }

        private async Task EnableBadges(MentorshipUpdateEvent notification)
        {
            if (notification.Status == MentorshipStatus.Accepted)
            {
                await mediator.PublishAsync(
                    new BadgeActivateEvent
                        {
                            ProfileId = notification.MenteeId,
                            BadgeType = BadgeType.FoundAMentor
                        });
               
            }
        }
    }
}