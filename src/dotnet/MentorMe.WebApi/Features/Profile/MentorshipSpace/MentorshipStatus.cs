﻿namespace MentorMe.WebApi.Features.Profile.MentorshipSpace
{
    public enum MentorshipStatus
    {
        Requested = 1,

        Accepted = 2,

        Declined = 3
    }
}