﻿namespace MentorMe.WebApi.Features.Profile.Notification
{
    using System.Collections.Generic;

    using MediatR;

    internal class RequestForAllNotifications : IRequest<IEnumerable<ResponseForAllNotifications>>
    {
        public string ProfileId { get; set; }
    }

    internal class ResponseForAllNotifications
    {
        public string Type { get; set; }

        public string Message { get; set; }

        public string Action { get; set; }
    }

    internal class RequestForAllEventsHandler :
        IRequestHandler<RequestForAllNotifications, IEnumerable<ResponseForAllNotifications>>
    {
        private readonly ProfileRepository profileRepository;

        public RequestForAllEventsHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public IEnumerable<ResponseForAllNotifications> Handle(RequestForAllNotifications message)
        {
            var profile = profileRepository.GetById(message.ProfileId);

            //foreach (var requestedChallenge in profile.Mentor.RequestedChallengesFromMentees)
            //{
            //    var mentee = profile.Mentor.
            //    yield return
            //        new ResponseForAllNotifications
            //            {
            //                Type = "RequestedChallengesFromMentees",
            //                Message =
            //                    $"You're challage {requestedChallenge.ChallengeId} was requested by {requestedChallenge.MenteeId}",
            //                Action = "See the callanges page!"
            //            };
            //}

            return null;
        }
    }
}