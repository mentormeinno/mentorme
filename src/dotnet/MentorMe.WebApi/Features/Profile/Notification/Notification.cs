﻿namespace MentorMe.WebApi.Features.Profile.Notification
{
    public class Notification
    {
        public string Message { get; set; }

        public string Type { get; set; }

        public string Id { get; set; }

        public string Action { get; set; }
    }
}