﻿namespace MentorMe.WebApi.Features.Profile.Notification
{
    using System.Collections.Generic;

    public static class ProfileExtensions
    {
        public static IEnumerable<Notification> GetNotifications(this Profile profile)
        {
            foreach (var requestedChallenge in profile.Mentor.RequestedChallengesFromMentees)
            {
                yield return new Notification
                        {
                            Type = "RequestedChallengesFromMentees",
                            Message =
                                $"You're challenge {requestedChallenge.ChallengeId} was requested by {requestedChallenge.MenteeId}",
                            Action = "See the callanges page!"
                        };
            }
        }
    }
}