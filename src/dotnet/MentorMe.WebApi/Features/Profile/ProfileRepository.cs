﻿namespace MentorMe.WebApi.Features.Profile
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using MongoRepository;

    public class ProfileRepository : MongoRepository<Profile>
    {
        public Profile GetProfileByEmail(string email)
        {
            return Collection.FindAll().FirstOrDefault(pr => pr.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
        }

        public IEnumerable<Profile> GetMentorsByTagForPage(int page, string tag, string menteeId)
        {
            var mentors = Collection.FindAll()
                    .Where(
                        pr =>
                            pr.Tags.ContainsKey(tag)
                            && pr.Mentor.MyMentees.All(mr => mr.MenteeId != menteeId)
                            && pr.Id != menteeId).OrderByDescending(pr => pr.Reputation)
                    .ToList();

            var totalRecords = mentors.Count();
            const int MentorsPerPage = 2;
            var maxPageNumber = (totalRecords + MentorsPerPage) / MentorsPerPage;

            if (maxPageNumber < page)
            {
                page = (int)maxPageNumber;
            }

            page = page > 0 ? page - 1 : page;
            IEnumerable<Profile> mentorsByPage = mentors.Skip(MentorsPerPage * page).Take(MentorsPerPage);
            return mentorsByPage;
        }

        public IEnumerable<Profile> GetProfilesWithHighRankOn(List<string> tagsCollection, string id)
        {
            if (tagsCollection.Count == 0)
            {
                return Collection.FindAll().Where(pr => pr.Id != id).OrderBy(pr => pr.Reputation).Take(5).ToList();
            }

            var profilesWithSelectedTags = Collection.FindAll().Where(pr => ContainsKeys(pr.Tags, tagsCollection) && pr.Id != id).OrderBy(pr => pr.Reputation).ToList();
            return profilesWithSelectedTags.Take(5);
        }

        public static bool ContainsKeys(Dictionary<string, decimal> dictionary, List<string> keys)
        {
            return keys.Any(dictionary.ContainsKey);
        }
    }
}