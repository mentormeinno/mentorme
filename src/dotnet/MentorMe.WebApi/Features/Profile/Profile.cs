﻿namespace MentorMe.WebApi.Features.Profile
{
    using System.Collections.Generic;
    using System.Linq;

    using MentorMe.WatsonService.Wrapper;
    using MentorMe.WebApi.Features.Badge;
    using MentorMe.WebApi.Features.Profile.MentorshipSpace;

    using MongoRepository;

    public class Profile : Entity
    {
        public Profile()
        {
            Tags = new Dictionary<string, decimal>();

            BadgeStash = new List<Badge>();

            Mentee = new Mentee
                         {
                             ResolvedChallengeIds = new List<string>(),
                             ChallengeTakenUpIds = new List<string>(),
                             MyMentors = new List<Mentorship>()
                         };

            Mentor = new Mentor
                         {
                             RequestedChallengesFromMentees = new List<RequestedChallenge>(),
                             ChallengeIds = new List<string>(),
                             MyMentees = new List<Mentorship>()
                         };
        }

        public Mentor Mentor { get; set; }

        public Mentee Mentee { get; set; }

        public List<Badge> BadgeStash { get; set; }

        public string Email { get; set; }

        public decimal Reputation
        {
            get
            {
                return Tags.Sum(x => x.Value);
            }
        }

        public Dictionary<string, decimal> Tags { get; set; }

        internal Dictionary<string, decimal> GetTagsSortedByReputation()
        {
            return Tags.OrderBy(x => x.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        internal void IncreaseReputation(decimal value, IList<string> tags)
        {
            var ponderedValue = (decimal)value / tags.Count;

            foreach (var tag in tags)
            {
                if (Tags.ContainsKey(tag))
                {
                    Tags[tag] += ponderedValue;
                }
                else
                {
                    Tags[tag] = ponderedValue;
                }
            }
        }

        internal void IncreaseReputation(decimal participantReputation, IList<string> tags, NlpResponse feedback)
        {
            var value = (decimal)0.01 * participantReputation;
            value += feedback.SentimentScore;
            value += feedback.JoyValue;
            value -= feedback.SadnessValue;
            value -= feedback.AngerValue;
            value -= feedback.DisgustValue;
            value -= feedback.FearValue;
            IncreaseReputation(value, tags);
        }
    }
}