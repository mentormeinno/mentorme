﻿namespace MentorMe.WebApi.Features.Profile.Extensions
{
    using System.Linq;

    using Challenge.Response;

    using MentorshipSpace;

    public static class MenteeExtensions
    {
        public static void AddTakeupChallengeRequestToMentee(this Profile profile, string challengeId)
        {
            if (!profile.Mentee.ResolvedChallengeIds.Contains(challengeId))
            {
                profile.Mentee.ChallengeTakenUpIds.Add(challengeId);
            }
        }

        public static void AddMentorshipRequestForMentee(this Profile menteeProfile, string mentorId, string tag)
        {
            if (!menteeProfile.Mentee.MyMentors.Exists(t => t.MentorId == mentorId))
            {
                menteeProfile.Mentee.MyMentors.Add(new Mentorship
                {
                    MenteeId = menteeProfile.Id,
                    MentorId = mentorId,
                    SearchedTag = tag,
                    Status = MentorshipStatus.Requested
                });
            }
        }

        public static void UpdateMentorshipResponseForMentee(this Profile menteeProfile, string mentorId, MentorshipStatus status, string rejectionReason)
        {
            Mentorship mentorship = menteeProfile.Mentee.MyMentors.First(x => x.MentorId == mentorId);
            mentorship.Status = status;
            mentorship.RejectionReason = rejectionReason;
        }

        public static void AddChallengeSubmission(this Profile profile, string challengeId, string reply)
        {
            if (!profile.Mentee.ResolvedChallengeIds.Contains(challengeId))
            {
                profile.Mentee.ChallengeSubmissions.Add(new ChallengeSubmission
                {
                    ChallengeId = challengeId,
                    Reply = reply
                });
            }
        }

        public static void RemovePendingChallenge(this Profile profile, string challengeId)
        {
            if (!profile.Mentee.ChallengeTakenUpIds.Contains(challengeId)) return;
            profile.Mentee.ResolvedChallengeIds.Add(challengeId);
            profile.Mentee.ChallengeTakenUpIds.Remove(challengeId);
        }
    }
}