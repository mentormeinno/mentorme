﻿namespace MentorMe.WebApi.Features.Profile.Extensions
{
    using System.Linq;

    using MentorMe.WebApi.Features.Challenge.Response;
    using MentorMe.WebApi.Features.Profile.MentorshipSpace;

    public static class MentorExtensions
    {
        public static void AddTakeupChallengeRequestToMentor(this Profile profile, string challengeId, string menteeId)
        {
            profile.Mentor.RequestedChallengesFromMentees.Add(
                new RequestedChallenge { MenteeId = menteeId, ChallengeId = challengeId });
        }

        public static void ReceiveChallengeSubmission(this Profile profile, ChallengeSubmission challengeSubmission)
        {
            if (!profile.Mentor.ChallengeSubmissions.Contains(challengeSubmission))
            {
                profile.Mentor.ChallengeSubmissions.Add(challengeSubmission);
            }
        }

        public static void AddMentorshipRequestForMentor(this Profile mentorProfile, string menteeId, string tag)
        {
            if (!mentorProfile.Mentor.MyMentees.Exists(t => t.MenteeId == menteeId))

                mentorProfile.Mentor.MyMentees.Add(new Mentorship
                {
                    MenteeId = menteeId,
                    MentorId = mentorProfile.Id,
                    SearchedTag = tag,
                    Status = MentorshipStatus.Requested
                });
        }

        public static void UpdateMentorshipResponseForMentor(this Profile mentorProfile, string menteeId, MentorshipStatus status, string rejectionReason)
        {
            Mentorship mentorship = mentorProfile.Mentor.MyMentees.First(x => x.MenteeId == menteeId);
            mentorship.Status = status;
            mentorship.RejectionReason = rejectionReason;
        }
    }
}