namespace MentorMe.WebApi.Features.Profile.Reputation.Add
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using MediatR;

    using MentorMe.WatsonService.Wrapper;

    internal class IncreaseReputationOfParticipant : IAsyncNotification
    {
        public string ParticipantId { get; set; }

        public string PresenterId { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public NlpResponse Feedback { get; set; }
    }

    internal class IncreaseReputationOfParticipantHandler : IAsyncNotificationHandler<IncreaseReputationOfParticipant>
    {
        private const decimal AttendFeedbackReputation = 0.5m;

        private readonly ProfileRepository profileRepository;

        public IncreaseReputationOfParticipantHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(IncreaseReputationOfParticipant notification)
        {
            var profile = profileRepository.GetById(notification.ParticipantId);
            profile.IncreaseReputation(AttendFeedbackReputation, notification.Tags.ToList());
            profileRepository.Update(profile);

            return Task.FromResult(true);
        }
    }
}