namespace MentorMe.WebApi.Features.Profile.Reputation.Add
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using MediatR;

    using MentorMe.WatsonService.Wrapper;

    internal class IncreaseReputationOfPresenter : IAsyncNotification
    {
        public string PresenterId { get; set; }

        public string ParticipantId { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public NlpResponse PresenterFeedback { get; set; }
    }

    internal class AddReputationOnTagsHandler : IAsyncNotificationHandler<IncreaseReputationOfPresenter>
    {
        private readonly ProfileRepository profileRepository;

        public AddReputationOnTagsHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(IncreaseReputationOfPresenter notification)
        {
            var participantReputation = profileRepository.GetById(notification.ParticipantId).Reputation;

            var profile = profileRepository.GetById(notification.PresenterId);

            profile.IncreaseReputation(participantReputation, notification.Tags.ToList(), notification.PresenterFeedback);

            profileRepository.Update(profile);

            return Task.FromResult(true);
        }
    }
}