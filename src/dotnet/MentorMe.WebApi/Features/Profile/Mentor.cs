﻿namespace MentorMe.WebApi.Features.Profile
{
    using System.Collections.Generic;

    using MentorMe.WebApi.Features.Challenge.Response;

    using MentorshipSpace;

    public class Mentor
    {
        public Mentor()
        {
            MyMentees = new List<Mentorship>();
            RequestedChallengesFromMentees = new List<RequestedChallenge>();
            ChallengeSubmissions = new List<ChallengeSubmission>();
        }

        public List<string> ChallengeIds { get; set; }

        public List<Mentorship> MyMentees { get; set; }

        public List<RequestedChallenge> RequestedChallengesFromMentees { get; set; }

        public List<ChallengeSubmission> ChallengeSubmissions { get; set; }
    }

    public class RequestedChallenge
    {
        public string MenteeId { get; set; }

        public string ChallengeId { get; set; }
    }
}