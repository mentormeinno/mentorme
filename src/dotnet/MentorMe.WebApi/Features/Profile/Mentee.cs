﻿namespace MentorMe.WebApi.Features.Profile
{
    using System.Collections.Generic;

    using Challenge.Response;
    using MentorshipSpace;

    public class Mentee
    {
        public Mentee()
        {
            MyMentors = new List<Mentorship>();
            ChallengeSubmissions = new List<ChallengeSubmission>();
        }

        public List<string> ChallengeTakenUpIds { get; set; }

        public List<Mentorship> MyMentors { get; set; }

        public List<ChallengeSubmission> ChallengeSubmissions { get; set; }

        public List<string> ResolvedChallengeIds { get; set; }
    }
}