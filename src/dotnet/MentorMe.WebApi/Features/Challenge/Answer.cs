﻿namespace MentorMe.WebApi.Features.Challenge
{
    using System;

    using MongoRepository;

    public class Answer : Entity
    {
        public int AnswerId { get; set; }

        public int ChallengeId { get; set; }

        public string AnswerText { get; set; }

        public Guid AnsweredBy { get; set; }
    }
}