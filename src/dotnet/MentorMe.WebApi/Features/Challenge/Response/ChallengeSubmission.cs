﻿namespace MentorMe.WebApi.Features.Challenge.Response
{
    public class ChallengeSubmission
    {
        public string ChallengeId { get; set; }

        public string MenteeId { get; set; }

        public string Reply { get; set; }

        public ChallengeSubmissionStatus SubmissionStatus { get; set; }
    }

    public enum ChallengeSubmissionStatus 
    {
        Pending=0,
        Accepted=1,
        Rejected=2
    }
}