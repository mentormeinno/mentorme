﻿namespace MentorMe.WebApi.Features.Challenge.Response
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MediatR;

    using Extensions;
    using Profile;

    public class IncreaseReputationHandler : IAsyncNotificationHandler<AcceptChallengeSubmissionEvent>
    {
        private readonly ChallengeRepository challengeRepository;
        private readonly ProfileRepository profileRepository;

        public IncreaseReputationHandler(ChallengeRepository challengeRepository, ProfileRepository profileRepository)
        {
            this.challengeRepository = challengeRepository;
            this.profileRepository = profileRepository;
        }

        public Task Handle(AcceptChallengeSubmissionEvent notification)
        {
            var challenge = challengeRepository.GetById(notification.ChallengeId);
            var pointToIncrease = challenge.CalculateReputationValueToBeAdded();
            List<string> tagsToBeIncreased = challenge.Tags;
            var mentee = profileRepository.GetById(notification.MenteeId);
            mentee.IncreaseReputation(pointToIncrease, tagsToBeIncreased);
            profileRepository.Update(mentee);
            return Task.FromResult(true);
        }
    }
}