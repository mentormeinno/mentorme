﻿namespace MentorMe.WebApi.Features.Challenge.Response
{
    using MediatR;

    public class SubmitChallengeResponseForReviewEvent : IAsyncNotification
    {
        public string ChallengeId { get; set; }

        public string MenteeId { get; set; }

        public string Reply { get; set; }
    }
}