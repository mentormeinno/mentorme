﻿using System.Linq;

namespace MentorMe.WebApi.Features.Challenge.Response
{
    using System.Threading.Tasks;

    using MediatR;

    using Profile;

    public class AcceptChallengeSubmissionHandler : IAsyncNotificationHandler<AcceptChallengeSubmissionEvent>
    {
        private readonly ProfileRepository profileRepository;

        public AcceptChallengeSubmissionHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(AcceptChallengeSubmissionEvent notification)
        {
            var mentee = profileRepository.GetById(notification.MenteeId);
            var mentor = profileRepository.Collection
                .FindAll().First(p => p.Mentor.ChallengeIds.Contains((notification.ChallengeId)));

            mentee.Mentee.ChallengeSubmissions.Find(cs => cs.ChallengeId == notification.ChallengeId).
                SubmissionStatus = ChallengeSubmissionStatus.Accepted;
            mentor.Mentor.ChallengeSubmissions.Find(cs => cs.ChallengeId == notification.ChallengeId).
                SubmissionStatus = ChallengeSubmissionStatus.Accepted;

            return Task.FromResult(true);
        }
    }
}