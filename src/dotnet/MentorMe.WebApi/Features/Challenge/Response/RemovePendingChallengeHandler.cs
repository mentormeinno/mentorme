﻿namespace MentorMe.WebApi.Features.Challenge.Response
{
    using System.Threading.Tasks;

    using MediatR;

    using Profile;
    using Profile.Extensions;

    public class RemovePendingChallengeHandler : IAsyncNotificationHandler<SubmitChallengeResponseForReviewEvent>
    {
        private readonly ProfileRepository profileRepository;

        public RemovePendingChallengeHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(SubmitChallengeResponseForReviewEvent notification)
        {
            var menteeProfile = profileRepository.GetById(notification.MenteeId);

            menteeProfile.RemovePendingChallenge(notification.ChallengeId);

            profileRepository.Update(menteeProfile);

            return Task.FromResult(true);
        }
    }
}