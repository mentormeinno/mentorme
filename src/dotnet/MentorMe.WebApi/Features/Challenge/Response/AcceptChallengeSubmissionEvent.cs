﻿namespace MentorMe.WebApi.Features.Challenge.Response
{
    using MediatR;

    public class AcceptChallengeSubmissionEvent : IAsyncNotification
    {
        public string ChallengeId { get; set; }

        public string MenteeId { get; set; }
    }
}