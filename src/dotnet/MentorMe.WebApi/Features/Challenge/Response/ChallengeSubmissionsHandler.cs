﻿namespace MentorMe.WebApi.Features.Challenge.Response
{
    using System.Threading.Tasks;

    using MediatR;

    using Profile;
    using Profile.Extensions;

    public class ChallengeSubmissionsHandler : IAsyncNotificationHandler<SubmitChallengeResponseForReviewEvent>
    {
        private readonly ProfileRepository profileRepository;

        private readonly ChallengeRepository challengeRepository;

        public ChallengeSubmissionsHandler(ProfileRepository profileRepository, ChallengeRepository challengeRepository)
        {
            this.profileRepository = profileRepository;
            this.challengeRepository = challengeRepository;
        }

        public Task Handle(SubmitChallengeResponseForReviewEvent notification)
        {
            var menteeProfile = profileRepository.GetById(notification.MenteeId);

            menteeProfile.AddChallengeSubmission(notification.ChallengeId, notification.Reply);

            profileRepository.Update(menteeProfile);

            var challenge = challengeRepository.GetById(notification.ChallengeId);
            var mentorProfile = profileRepository.GetById(challenge.AddedBy);

            mentorProfile.ReceiveChallengeSubmission(new ChallengeSubmission
            {
                ChallengeId =
                notification.ChallengeId,
                Reply = notification.Reply,
                MenteeId = notification.MenteeId,
                SubmissionStatus = ChallengeSubmissionStatus.Pending
            });
            profileRepository.Update(mentorProfile);
            return Task.FromResult(true);
        }

     
    }
}