﻿namespace MentorMe.WebApi.Features.Challenge.TakeUp
{
    using System.Threading.Tasks;

    using MediatR;

    using Profile;
    using Profile.Extensions;

    public class UpdateMentorTakeupChallengeHandler : IAsyncNotificationHandler<TakeupChallengeEvent>
    {
        private readonly ProfileRepository profileRepository;

        public UpdateMentorTakeupChallengeHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(TakeupChallengeEvent notification)
        {
            var mentorProfile = profileRepository.GetById(notification.MentorId);
            mentorProfile.AddTakeupChallengeRequestToMentor(notification.ChallengeId, notification.MenteeId);
            profileRepository.Update(mentorProfile);

            return Task.FromResult(true);
        }
    }
}