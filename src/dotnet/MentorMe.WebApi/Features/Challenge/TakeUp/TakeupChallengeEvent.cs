﻿namespace MentorMe.WebApi.Features.Challenge.TakeUp
{
    using MediatR;

    public class TakeupChallengeEvent : IAsyncNotification
    {
        public string MenteeId { get; set; }

        public string MentorId { get; set; }

        public string ChallengeId { get; set; }
    }
}