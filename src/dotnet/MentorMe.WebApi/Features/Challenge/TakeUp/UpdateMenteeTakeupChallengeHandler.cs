﻿namespace MentorMe.WebApi.Features.Challenge.TakeUp
{
    using System.Threading.Tasks;

    using MediatR;

    using Profile;
    using Profile.Extensions;

    public class UpdateMenteeTakeupChallengeHandler : IAsyncNotificationHandler<TakeupChallengeEvent>
    {
        private readonly ProfileRepository profileRepository;

        public UpdateMenteeTakeupChallengeHandler(ProfileRepository profileRepository)
        {
            this.profileRepository = profileRepository;
        }

        public Task Handle(TakeupChallengeEvent notification)
        {
            var menteeProfile = profileRepository.GetById(notification.MenteeId);
            menteeProfile.AddTakeupChallengeRequestToMentee(notification.ChallengeId);
            profileRepository.Update(menteeProfile);

            return Task.FromResult(true);
        }
    }
}