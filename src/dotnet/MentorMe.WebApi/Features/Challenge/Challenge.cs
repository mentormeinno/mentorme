﻿namespace MentorMe.WebApi.Features.Challenge
{
    using System;
    using System.Collections.Generic;

    using MongoRepository;

    public class Challenge : Entity
    {
        public string ChallengeText { get; set; }

        public string Type { get; set; }

        public string AddedBy { get; set; }

        public List<string> Tags { get; set; }

        public int MinimumLevel { get; set; }

        public int Difficulty { get; set; }
    }
}