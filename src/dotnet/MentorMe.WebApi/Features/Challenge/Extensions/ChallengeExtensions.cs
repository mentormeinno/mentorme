﻿namespace MentorMe.WebApi.Features.Challenge.Extensions
{
    public static class ChallengeExtensions
    {
        private const decimal MAXIMUM_VALUE_CHALLENGE = 10;
        public static decimal CalculateReputationValueToBeAdded(this Challenge challenge)
        {
            return (MAXIMUM_VALUE_CHALLENGE * challenge.Difficulty) / 100;
        }
    }
}