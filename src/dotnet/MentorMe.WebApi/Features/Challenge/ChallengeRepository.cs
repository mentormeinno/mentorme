﻿namespace MentorMe.WebApi.Features.Challenge
{
    using System.Collections.Generic;
    using System.Linq;

    using MongoRepository;

    public class ChallengeRepository : MongoRepository<Challenge>
    {
        public IEnumerable<Challenge> GetChallengesByIds(List<string> mentorChallengesIds)
        {
            IEnumerable<Challenge> challenges = Collection.FindAll().Where(ch => mentorChallengesIds.Contains(ch.Id));
            return challenges;
        }
    }
}