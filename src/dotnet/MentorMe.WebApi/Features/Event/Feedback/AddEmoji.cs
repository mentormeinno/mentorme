﻿namespace MentorMe.WebApi.Features.Event.Feedback
{
    using System.Threading.Tasks;

    using MediatR;

    public class AddEmojiToFeedback : IAsyncNotification
    {
        public string Emoji { get; set; }

        public string EventId { get; set; }
    }

    public class AddEmojiToFeedbackHandler : IAsyncNotificationHandler<AddEmojiToFeedback>
    {
        private readonly EventRepository eventRepository;

        public AddEmojiToFeedbackHandler(EventRepository eventRepository)
        {
            this.eventRepository = eventRepository;
        }

        public Task Handle(AddEmojiToFeedback notification)
        {
            var ev = eventRepository.GetById(notification.EventId);
            ev.Emojies.Add(notification.Emoji);
            eventRepository.Update(ev);

            return Task.FromResult(true);
        }
    }
}