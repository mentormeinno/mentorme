﻿namespace MentorMe.WebApi.Features.Event.Query
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MediatR;

    internal class RequestEventDetails : IAsyncRequest<ResponseEventTags>
    {
        public string EventId { get; set; }
    }

    internal class ResponseEventTags
    {
        public string EventId { get; set; }

        public string PresenterId { get; set; }

        public IEnumerable<string> Tags { get; set; }
    }

    internal class RequestEventTagsHandler : IAsyncRequestHandler<RequestEventDetails, ResponseEventTags>
    {
        private readonly EventRepository eventRepository;

        public RequestEventTagsHandler(EventRepository eventRepository)
        {
            this.eventRepository = eventRepository;
        }

        public async Task<ResponseEventTags> Handle(RequestEventDetails message)
        {
            var ev = eventRepository.GetById(message.EventId);
            return await Task.Run(() => new ResponseEventTags { EventId = ev.Id, PresenterId = ev.PresenterId, Tags = ev.Tags });
        }
    }
}