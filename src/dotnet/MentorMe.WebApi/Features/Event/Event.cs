﻿namespace MentorMe.WebApi.Features.Event
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using MongoRepository;

    public class Event : Entity
    {
        public Event()
        {
            Emojies = new List<string>();
        }

        public string Name { get; set; }

        public string Type { get; set; }

        public string PresenterId { get; set; }

        public List<string> Tags { get; set; }

        public List<string> Emojies { get; set; }

        public string Location { get; set; }

        public DateTime StartTime { get; set; }

        public Dictionary<string, int> EmojiesCount => Emojies.GroupBy(x => x).ToDictionary(k => k.Key, v => v.Count());
    }
}