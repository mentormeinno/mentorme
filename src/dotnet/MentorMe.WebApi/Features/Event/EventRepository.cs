﻿namespace MentorMe.WebApi.Features.Event
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using MongoRepository;

    public class EventRepository : MongoRepository<Event>
    {
        public IEnumerable<Event> GetAllEventsFor(string profileId)
        {
            return Collection.FindAll().Where(pr => pr.PresenterId == profileId);
        }

        public IEnumerable<Event> GetAllEventsStartingFrom(DateTime dateTime)
        {
            return Collection.FindAll().Where(ev => ev.StartTime >= dateTime);
        }

        public IEnumerable<Event> GetAllEventsInLocation(string location)
        {
            return Collection.FindAll().Where(ev => ev.Location.Equals(location, StringComparison.OrdinalIgnoreCase));
        }
    }
}