﻿namespace MentorMe.WebApi.Features.Badge
{
    using MediatR;

    public class BadgeActivateEvent : IAsyncNotification
    {
        public string ProfileId { get; set; }

        public BadgeType BadgeType { get; set; }
    }
}