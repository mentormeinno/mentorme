﻿namespace MentorMe.WebApi.Features.Badge
{
    using System;

    using MongoRepository;

    public class Badge : Entity
    {
        public BadgeType Type { get; set; }

        public int Level { get; set; }

        public int Value { get; set; }

        public string Name { get; set; }

        public string TypeDescription { get; set; }

        public static BadgeType ParseEnum(string value)
        {
            return (BadgeType)Enum.Parse(typeof(BadgeType), value, ignoreCase: true);
        }
    }
}