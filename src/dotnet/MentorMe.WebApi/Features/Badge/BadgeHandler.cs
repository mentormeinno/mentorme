﻿namespace MentorMe.WebApi.Features.Badge
{
    using System.Linq;
    using System.Threading.Tasks;

    using MediatR;

    using MentorMe.WebApi.Features.Profile;

    public class BadgeHandler : IAsyncNotificationHandler<BadgeActivateEvent>
    {
        private readonly BadgeRepository badgeRepository;

        private readonly ProfileRepository profileRepository;

        public BadgeHandler(BadgeRepository badgeRepository, ProfileRepository profileRepository)
        {
            this.badgeRepository = badgeRepository;
            this.profileRepository = profileRepository;
        }

        public Task Handle(BadgeActivateEvent notification)
        {
            switch (notification.BadgeType)
            {
                case BadgeType.FoundAMentee:
                    ActivateFoundAMentee(notification.ProfileId);
                    break;
                case BadgeType.FoundAMentor:
                    ActivateFoundAMentor(notification.ProfileId);
                    break;
                case BadgeType.ShareAChallenge:
                    ActivateShareAChallenge(notification.ProfileId);
                    break;
                case BadgeType.TakeUpChallenge:
                    ActivateTakeUpChallenge(notification.ProfileId);
                    break;
            }

            return Task.FromResult(true);
        }

        private void ActivateTakeUpChallenge(string notificationProfileId)
        {
            CheckIfBadgeNeedsToBeAddedForCategory(notificationProfileId, BadgeType.TakeUpChallenge);
        }

        private void ActivateShareAChallenge(string notificationProfileId)
        {
            CheckIfBadgeNeedsToBeAddedForCategory(notificationProfileId, BadgeType.ShareAChallenge);
        }

        private void ActivateFoundAMentor(string notificationProfileId)
        {
            CheckIfBadgeNeedsToBeAddedForCategory(notificationProfileId, BadgeType.FoundAMentor);
        }

        private void ActivateFoundAMentee(string notificationProfileId)
        {
            CheckIfBadgeNeedsToBeAddedForCategory(notificationProfileId, BadgeType.FoundAMentee);
        }

        private void CheckIfBadgeNeedsToBeAddedForCategory(string notificationProfileId, BadgeType badgeType)
        {
            var profile = profileRepository.GetById(notificationProfileId);
            if (!profile.BadgeStash.Exists(bs => bs.Type == badgeType)) profile.BadgeStash.Add(badgeRepository.Collection.FindAll().Single(bs => bs.Type == badgeType));
            profileRepository.Update(profile);
        }
    }
}