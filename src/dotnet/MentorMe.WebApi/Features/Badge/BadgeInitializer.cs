﻿namespace MentorMe.WebApi.Features.Badge
{
    using System;
    using System.Linq;
    using System.Xml.Linq;

    public class BadgeInitializer
    {
        private readonly BadgeRepository repository;

        public BadgeInitializer(BadgeRepository badgeRepository)
        {
            repository = badgeRepository;
        }

        public void Init()
        {
            repository.DeleteAll();

            var xdoc =
                XDocument.Parse(
                    @"<Badges><Badge Type = ""NoTagsWithReputation"" TypeDescription = ""Number of tags with reputation"" Level = ""1"" Value = ""5"" Name = ""Beginner tag hunter"" /><Badge Type = ""NoTagsWithReputation"" TypeDescription = ""Number of tags with reputation"" Level = ""2"" Value = ""10"" Name = ""Advanced tag hunter"" /><Badge Type = ""NoTagsWithReputation"" TypeDescription = ""Number of tags with reputation"" Level = ""3"" Value = ""15"" Name = ""Expert tag hunter"" /><Badge Type = ""ReputationOnTag"" TypeDescription = ""Reputation on tag"" Level = ""1"" Value = ""10"" Name = ""Beginner reputation builder"" /><Badge Type = ""ReputationOnTag"" TypeDescription = ""Reputation on tag"" Level = ""2"" Value = ""100"" Name = ""Advanced reputation builder"" /><Badge Type = ""ReputationOnTag"" TypeDescription = ""Reputation on tag"" Level = ""3"" Value = ""1000"" Name = ""Expert reputation builder"" /><Badge Type = ""FoundAMentor"" TypeDescription = ""Found a mentor"" Level = ""1"" Value = ""1"" Name = ""Sensei, there you are"" /><Badge Type = ""FoundAMentee"" TypeDescription = ""Found a mentee"" Level = ""1"" Value = ""1"" Name = ""The force is strong with this one"" /><Badge Type = ""TakeUpChallenge"" TypeDescription = ""Take up challenge"" Level = ""1"" Value = ""1"" Name = ""Much to learn have you"" /><Badge Type = ""NoMentees"" TypeDescription = ""No of mentees"" Level = ""1"" Value = ""5"" Name = ""Minions"" /><Badge Type = ""NoMentees"" TypeDescription = ""No of mentees"" Level = ""2"" Value = ""10"" Name = ""More than a scrum"" /><Badge Type = ""NoMentees"" TypeDescription = ""No of mentees"" Level = ""3"" Value = ""15"" Name = ""Wolf pack"" /><Badge Type = ""ShareAChallenge"" TypeDescription = ""Share a challenge"" Level = ""1"" Value = ""1"" Name = ""Are you strong enough"" /><Badge Type = ""ResolveChallenge"" TypeDescription = ""Resolve challenge"" Level = ""1"" Value = ""1"" Name = ""Piece of cake"" /><Badge Type = ""ResolveChallenge"" TypeDescription = ""Resolve challenge"" Level = ""2"" Value = ""5"" Name = ""Just getting started"" /><Badge Type = ""ResolveChallenge"" TypeDescription = ""Resolve challenge"" Level = ""3"" Value = ""10"" Name = ""Nutcracker"" /><Badge Type = ""ResolveChallenge"" TypeDescription = ""Resolve challenge"" Level = ""4"" Value = ""15"" Name = ""Master troubleshooter"" /><Badge Type = ""CreateEvent"" TypeDescription = ""Create event"" Level = ""1"" Value = ""1"" Name = ""Go out into the world"" /><Badge Type = ""CreateEvent"" TypeDescription = ""Create event"" Level = ""2"" Value = ""20"" Name = ""Sharing is caring"" /><Badge Type = ""CreateEvent"" TypeDescription = ""Create event"" Level = ""3"" Value = ""50"" Name = ""Event planner"" /><Badge Type = ""ParticipateEvent"" TypeDescription = ""Participate event"" Level = ""1"" Value = ""1"" Name = ""Part of the in-crowd"" /><Badge Type = ""ParticipateEvent"" TypeDescription = ""Participate event"" Level = ""2"" Value = ""20"" Name = ""Front row seater"" /><Badge Type = ""ParticipateEvent"" TypeDescription = ""Participate event"" Level = ""3"" Value = ""50"" Name = ""Communities are great"" /><Badge Type = ""AllEventTypesCreated"" TypeDescription = ""All event types created"" Level = ""1"" Value = ""0"" Name = ""There is nothing I cannot do"" /><Badge Type = ""AllEventTypesAttended"" TypeDescription = ""All event types attended"" Level = ""1"" Value = ""0"" Name = ""I want to know it all"" /></Badges>");
            //Run query
            var badgesElements = from lv1 in xdoc.Descendants("Badge")
                                 select
                                 new
                                     {
                                         Type = lv1.Attribute("Type").Value,
                                         TypeDescription = lv1.Attribute("TypeDescription").Value,
                                         Level = lv1.Attribute("Level").Value,
                                         Value = lv1.Attribute("Value").Value,
                                         Name = lv1.Attribute("Name").Value
                                     };

            //Loop through results
            foreach (var item in badgesElements)
            {
                repository.Add(
                    new Badge
                        {
                            Type = (BadgeType)Enum.Parse(typeof(BadgeType), item.Type, ignoreCase: true),
                            TypeDescription = item.TypeDescription,
                            Level = Convert.ToInt32(item.Level),
                            Value = Convert.ToInt32(item.Value),
                            Name = item.Name
                        });
            }
        }
    }
}