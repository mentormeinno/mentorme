﻿namespace MentorMe.WebApi.Features.Badge
{
    public enum BadgeType
    {
        NoTagsWithReputation = 0,

        ReputationOnTag = 1,

        FoundAMentor = 2,

        FoundAMentee = 3,

        TakeUpChallenge = 4,

        NoMentees = 5,

        ShareAChallenge = 6,

        ResolveChallenge = 7,

        CreateEvent = 8,

        ParticipateEvent = 9,

        AllEventTypesCreated = 10,

        AllEventTypesAttended = 11,

        SubmitChallengeForReview = 12
    }
}