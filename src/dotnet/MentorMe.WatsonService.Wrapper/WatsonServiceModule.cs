﻿namespace MentorMe.WatsonService.Wrapper
{
    using Autofac;

    using MentorMe.Configuration;

    public class WatsonServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(
                    ctx =>
                        {
                            var configProvider = ctx.Resolve<IAppConfigProvider>();

                            return new NluWatsonServiceWrapper(
                                new NluWatsonServiceConfig
                                    {
                                        Uri = configProvider.Get("Watson.Uri"),
                                        UserName = configProvider.Get("Watson.User"),
                                        Password = configProvider.Get("Watson.Pass"),
                                    });
                        })
                .As<INaturalLanguageProcessor>();
        }
    }
}