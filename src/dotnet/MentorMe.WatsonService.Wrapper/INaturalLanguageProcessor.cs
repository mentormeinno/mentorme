﻿namespace MentorMe.WatsonService.Wrapper
{
    using System.Net;
    using System.Threading.Tasks;

    public interface INaturalLanguageProcessor
    {
        Task<NlpResponse> Get(string text);
    }
}