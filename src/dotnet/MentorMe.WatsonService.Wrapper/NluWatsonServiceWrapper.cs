﻿namespace MentorMe.WatsonService.Wrapper
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    public class NlpResponse
    {
        public string SentimentLabel { get; set; }

        public decimal SentimentScore { get; set; }

        public decimal SadnessValue { get; set; }

        public decimal JoyValue { get; set; }

        public decimal FearValue { get; set; }

        public decimal DisgustValue { get; set; }

        public decimal AngerValue { get; set; }
    }

    public class NluWatsonServiceWrapper : INaturalLanguageProcessor
    {
        private readonly NluWatsonServiceConfig config;

        public NluWatsonServiceWrapper(NluWatsonServiceConfig config)
        {
            this.config = config;
        }

        public async Task<NlpResponse> Get(string text)
        {
            var features = new
                               {
                                   //concepts = new { keywords = true, emotion = true, sentiment = true, limit = 4 },
                                   emotion = new { keywords = true, sentiment = true, limit = 5 },
                                   //keywords = new { emotion = true, sentiment = true, limit = 4 },
                                   sentiment = new { keywords = true, emotion = true, limit = 5 },
                               };

            var postDataJson = JsonConvert.SerializeObject(new { text, features });

            byte[] byteArray = Encoding.UTF8.GetBytes(postDataJson);

            var request = WebRequest.Create(config.Uri);

            request.Method = "POST";
            request.Credentials = new NetworkCredential(config.UserName, config.Password);
            request.ContentLength = byteArray.Length;
            request.ContentType = "application/json";

            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var response = await request.GetResponseAsync();

            dataStream = response.GetResponseStream();

            if (dataStream == null)
            {
                throw new InvalidOperationException("Watson responed with null value!");
            }

            var reader = new StreamReader(dataStream);
            var responseFromServer = reader.ReadToEnd();

            reader.Close();
            dataStream.Close();
            response.Close();

            var res = JsonConvert.DeserializeObject<dynamic>(responseFromServer);

            return new NlpResponse
                       {
                           SentimentLabel = res.sentiment.document.label.ToString(),
                           SentimentScore = decimal.Parse(res.sentiment.document.score.ToString()),
                           SadnessValue = decimal.Parse(res.emotion.document.emotion.sadness.ToString()),
                           JoyValue = decimal.Parse(res.emotion.document.emotion.joy.ToString()),
                           FearValue = decimal.Parse(res.emotion.document.emotion.fear.ToString()),
                           DisgustValue = decimal.Parse(res.emotion.document.emotion.disgust.ToString()),
                           AngerValue = decimal.Parse(res.emotion.document.emotion.anger.ToString())
                       };
        }
    }
}