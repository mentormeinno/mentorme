﻿namespace MentorMe.WatsonService.Wrapper
{
    public class NluWatsonServiceConfig
    {
        public string Password { get; set; }

        public string UserName { get; set; }

        public string Uri { get; set; }
    }
}